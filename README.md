1. 參考資料
   - [高鐵一般訂票網頁](https://irs.thsrc.com.tw/IMINT)
   - [使用Keras基於TensorFlow和Python3.6 識別高鐵驗證碼](http://www.csie.ntnu.edu.tw/~40647027s/)


2. 安裝pytesseract
   - Mac

    ```bash
    pip install pytesseract
    pip install tesseract-ocr
    brew install leptonica
    
    ```